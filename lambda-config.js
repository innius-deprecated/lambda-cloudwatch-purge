module.exports = {
    region: 'us-east-1',
    handler: 'index.handler',
    functionName: "cloudwatch-logstream-purge",
    timeout: 300,
    memorySize: 128,
    publish: true,
    runtime: 'nodejs6.10',
    vpc: {
        SecurityGroupIds: [],
        SubnetIds: []
    }
}