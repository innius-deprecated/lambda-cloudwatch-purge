# Cloudwatch Purge Lambda
this function deletes all empty CloudWatch Log Streams which are outside the rentention policy. AWS does not delete these empty streams automatically. As a result finding the appropriate log stream my become a time consuming task. 

## Usage
the lambda expectes an event like this : 
```
{
    "logGroups" : [
        "/innius/chair", 
        "/innius/xenon"
    ]
}
```

