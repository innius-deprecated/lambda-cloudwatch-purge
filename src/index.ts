import { CloudWatchLogs } from "aws-sdk";
import * as log from "./logger";
import * as Rx from "rxjs/Rx";

let cloudwatchlogs = new CloudWatchLogs({ region: "us-east-1" });

interface Event { 
    logGroups : string[];
}

export function handler(event: Event | String, context: any, callback: (err?: any, data?: string) => void): void {
    if (typeof event === "string") {
        event = JSON.parse(event);
    }

    console.log(event);
    let stream = Rx.Observable
        .from(event.logGroups)
        .flatMap(x => describeGroups(x))
        .flatMap((x: CloudWatchLogs.LogGroup) => describeLogStreams(x))        
        .filter((x: LogStream) => {
            const filter = new Date().getTime() - (x.logGroup.retentionInDays * 86400000);
            return x.lastEventTimeStamp < filter;
        })                
        .flatMap((x: LogStream) => deleteLogStream(x),10)                
        .do(x => console.log(`log stream ${JSON.stringify(x)} deleted`))                
        .count();    

    stream.subscribe(
        (x) => console.log(`${x} streams were deleted`),
        (err) => {
            console.log(JSON.stringify(err));
            callback(err);
        },
        () => {
            console.log("done");
            callback();
        });
}

class LogStream {
    logGroup: CloudWatchLogs.LogGroup;
    logStreamName: string;
    lastEventTimeStamp: number;
}

function deleteLogStream(logStream: LogStream): Rx.Observable<LogStream> {
    let result :  Rx.Observable<LogStream> = Rx.Observable.create((obs: Rx.Observer<LogStream>) => {
        let params: CloudWatchLogs.DeleteLogStreamRequest = {
            logGroupName: logStream.logGroup.logGroupName,
            logStreamName: logStream.logStreamName
        };
        cloudwatchlogs.deleteLogStream(params, (err, data) => {
            if (err) {
                obs.error(err);
            } {
                obs.next(logStream);
            }

            obs.complete();
        });
    });

    return result.catch((err, foo) => foo);
}

function describeLogStreams(logGroup: CloudWatchLogs.LogGroup): Rx.Observable<LogStream> {
    return Rx.Observable.create((obs: Rx.Observer<CloudWatchLogs.LogStream>) => {

        function task(params: CloudWatchLogs.DescribeLogStreamsRequest): void {
            cloudwatchlogs.describeLogStreams(params, (err, data) => {
                if (err) {
                    obs.error(err);
                } else {
                    data.logStreams
                        .map(x => {
                            return {
                                logGroup: logGroup,
                                logStreamName: x.logStreamName,
                                lastEventTimeStamp: x.lastEventTimestamp
                            };                            
                        })
                        .forEach(x => obs.next(x));
                    if (data.nextToken) {
                        this.schedule(Object.assign({}, params, { nextToken: data.nextToken }), 500);
                    } else {
                        obs.complete();
                    }
                }
            });            
        }
        Rx.Scheduler.async.schedule<CloudWatchLogs.DescribeLogStreamsRequest>(task, 0 , { logGroupName: logGroup.logGroupName });        
    });
}

function describeGroups(logGroupNamePrefix: string): Rx.Observable<CloudWatchLogs.LogGroup> {
    return Rx.Observable.create((obs: Rx.Observer<CloudWatchLogs.LogGroup>) => {
        let params = {
            logGroupNamePrefix: logGroupNamePrefix
        };

        cloudwatchlogs.describeLogGroups(params, (err, data) => {
            if (err) {
                obs.error(err);
            } else {
                data.logGroups.forEach(x => obs.next(x));
            }
            obs.complete();
        });

    });
}

