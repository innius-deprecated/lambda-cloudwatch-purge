let currentLogLevel = 2;

function log(logLevel: number, message: string): void {
    if (logLevel >= currentLogLevel) {
        console.log(message);
    }
}

const level = {
    trace: 0,
    debug: 1,
    info: 2,
    error: 3
};

export function setLevel(logLevel: string): boolean {
    const found = level[logLevel];
    if (found !== undefined) {
        currentLogLevel = found;
    } else {
        error(`Unacceptable log level: ${logLevel}`);
    }

    return found !== undefined;
}

export function getLevel(): string {
    for (const lvl in level) {
        if (level[lvl] === currentLogLevel) {
            return lvl;
        }
    }

    return null;
}

export function error(message: string): void {
    log(level.error, message);
}

export function info(message: string): void {
    log(level.info, message);
}

export function debug(message: string): void {
    log(level.debug, message);
}

export function trace(message: string): void {
    log(level.trace, message);
}
